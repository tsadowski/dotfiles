Bunch of humble dotfiles
Personaly I keep symlinks in my ~ to those files.


# List of files
* zsh theme `xenosathl.zsh-theme`
* tmux configuration `.tmux.conf`
* polybar configuration with encoded unicode `polybar.conf`
* main colors palette `.Xresources`

# zsh theme
Personal colors (slightly modified Konsole `Dark Pastels` palette):
* Foreground: #dcdccc
* Background: #2c2c2c
* Black: #709080
* Red: #de6453
* Green: #97d56d
* Yellow: #f0dfaf
* Blue: #94bff3
* Magenta: #ec93d3
* Cyan: #93e0e3
* White: #ffffff

To use it move `xenosathl.zsh-theme` file to your `~/.oh-my-zsh/custom` directory and in your `~/.zshrc` file set `ZSH_THEME="xenosathl"`

# tmux conf
Not big deal:
* change prefix to Ctrl+a
* splitting panes horizontaly by `\`
* splitting panes verticaly by `-`
* automatically go to current path in new pane
* bigger pane history

